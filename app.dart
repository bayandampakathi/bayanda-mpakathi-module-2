class App {
  late String _appName;
  late String _catagory;
  late String _developer;
  late String _yearItWon;

  App({
    required String appName,
    required String catagory,
    required String developer,
    required String yearItWon,
  }) {
    _appName = appName;
    _catagory = catagory;
    _developer = developer;
    _yearItWon = yearItWon;

    print("App Name: $_appName");
    print("Catagory: $_catagory");
    print("Developer: $_developer");
    print("\n");
  }

  String getAppName() => _appName;
  String getCatagory() => _catagory;
  String getDeveloper() => _developer;
  String getYearItWon() => _yearItWon;

  void appNamesToUpperCase() {
    print(_appName.toUpperCase());
  }
}

void main() {
  App app1 = App(
    appName: "Takelot",
    catagory: "Best Consumer Solution",
    developer: "Takelot Developers",
    yearItWon: "2021",
  );
  App app2 = App(
    appName: "UniWise",
    catagory: "Best Campus Cup Solution",
    developer: "Colledge Students",
    yearItWon: "2021",
  );
  App app3 = App(
    appName: "FNB",
    catagory: "Best Financial Solution",
    developer: "FNB Developers",
    yearItWon: "2012",
  );
  print("\n App names in Capital letters\n");
  app1.appNamesToUpperCase();
  app2.appNamesToUpperCase();
  app3.appNamesToUpperCase();
}
